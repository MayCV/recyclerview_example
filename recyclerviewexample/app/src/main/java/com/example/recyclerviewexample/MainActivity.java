package com.example.recyclerviewexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.widget.LinearLayout;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Product> listProduct = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

        ProductAdapter adapter = new ProductAdapter(this, listProduct,this);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, LinearLayout.VERTICAL);

        RecyclerView rvProduct = (RecyclerView) findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);

    }
    private void initData(){
        listProduct.add(new Product("Smart Phone v1","New Product","6500000",R.drawable.img1));
        listProduct.add(new Product("Smart Phone v2","New Product","6600000",R.drawable.img2));
        listProduct.add(new Product("Smart Phone v3","New Product","6700000",R.drawable.img3));
        listProduct.add(new Product("Smart Phone v4","New Product","6800000",R.drawable.img4));
        listProduct.add(new Product("Smart Phone v5","New Product","6900000",R.drawable.img5));
        listProduct.add(new Product("Smart Phone v6","New Product","6100000",R.drawable.img6));
        listProduct.add(new Product("Smart Phone v7","New Product","6200000",R.drawable.img1));
        listProduct.add(new Product("Smart Phone v8","New Product","6300000",R.drawable.img2));
        listProduct.add(new Product("Smart Phone v9","New Product","6400000",R.drawable.img3));
        listProduct.add(new Product("Smart Phone v10","New Product","7000000",R.drawable.img4));
        listProduct.add(new Product("Smart Phone v11","New Product","7100000",R.drawable.img5));
        listProduct.add(new Product("Smart Phone v12","New Product","7200000",R.drawable.img6));
        listProduct.add(new Product("Smart Phone v13","New Product","7300000",R.drawable.img1));


    }
}
